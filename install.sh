#!/usr/bin/env bash
cd
if [ ! -d cat-mb6 ]
then
    mkdir cat-mb6
fi
cd cat-mb6
if [ ! -d src ]
then
    mkdir src
fi
git clone https://bitbucket.org/imt-mobisyst/mb6-tbot src/tbot
git -C src/tbot/ checkout master
bash src/tbot/install_dependencies.sh
catkin_make

Echo ">> TBOT (on branch: master) installed in ~/cat-mb6/src <<"
