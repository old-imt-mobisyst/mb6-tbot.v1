#!/usr/bin/env bash
sudo apt update

# Just in case:
sudo apt install -y git sshfs curl code build-essential

# Robuki:
sudo apt install -y \
    ros-noetic-ecl-exceptions \
    ros-noetic-ecl-threads \
    ros-noetic-ecl-geometry \
    ros-noetic-ecl-streams \
    ros-noetic-ecl-eigen \
    ros-noetic-kobuki-* \
    ros-noetic-depthimage-to-laserscan \
    ros-noetic-joy \
    ros-noetic-urg-node \
    ros-noetic-gazebo-ros \
    ros-noetic-gazebo-plugins \
    ros-noetic-depth-image-proc

# RealSense:
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE
sudo add-apt-repository "deb https://librealsense.intel.com/Debian/apt-repo $(lsb_release -cs) main" -u

## Independant:
sudo apt install -y \
    librealsense2-dkms \
    librealsense2-utils \
    librealsense2-dev \
    librealsense2-dbg

## In ROS:
sudo apt install -y \
    ros-noetic-librealsense2 \
    ros-noetic-realsense2-camera \
    ros-noetic-realsense2-description
    ros-noetic-depthimage-to-laserscan \
    ros-noetic-rqt \
    ros-noetic-rqt-image-view

# Navigation:
sudo apt install -y \
    ros-noetic-amcl \
    ros-noetic-move-base \
    ros-noetic-map-server \
    ros-noetic-gmapping
